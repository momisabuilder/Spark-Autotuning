package FileIO;
/**
*@author tonychao 
*功能 用于调用Linux shell 的函数，要求单线程，阻塞，将标准输出打印至屏幕，标准输入并不进行处理
*/
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class OutPutFile {
	String path=null;
	FileOutputStream out =null;
	public OutPutFile (String path) throws IOException {
		//创建并覆盖文件
		this.path=path;
		 File file=new File(path);
         if(!file.exists()) file.createNewFile();
		 this.out=new FileOutputStream(file,false); //直接覆盖之前的文件      
		

	}

	//文件末尾加入一个单词
	public void add(String word) throws UnsupportedEncodingException, IOException{
		if (word!=null) out.write(("\t"+word).getBytes("utf-8"));
		
	}
	
	//向文件末尾中加入一行
	public void addLine(String line) throws UnsupportedEncodingException, IOException{
		
        if(line!=null) out.write((line+"\n").getBytes("utf-8")); //linux中使用 \n即可 win中可以用 回车换行 \r\n
        
	}
	
	//手打开文件
	public void open(){
		
	}
	
	//手动关闭文件，这意味着文件写入结束
	public void close() throws IOException{
		this.out.close();
	}


}
