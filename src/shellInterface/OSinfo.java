/**
 * 
 */
package shellInterface;
/**
*用于判断系统的类型
* System.out.println(OSinfo.getOSname());
*创建时间 ：2017年7月18日 下午5:40:02
*/



public class OSinfo {  

    private static String OS = System.getProperty("os.name").toLowerCase();  
     	//不可以使用关键字this，因为this 和一个instance相关，所以不能使用在static 关键字修饰的函数中
    private static OSinfo _instance = new OSinfo();  
      
    private  OSType oSType;  
      
    private OSinfo(){}  
      
    public static boolean isLinux(){  
        return OS.indexOf("linux")>=0;  
    }  
      
    public static boolean isMacOS(){  
        return OS.indexOf("mac")>=0&&OS.indexOf("os")>0&&OS.indexOf("x")<0;  
    }  
      
    public static boolean isMacOSX(){  
        return OS.indexOf("mac")>=0&&OS.indexOf("os")>0&&OS.indexOf("x")>0;  
    }  
      
    public static boolean isWindows(){  
        return OS.indexOf("windows")>=0;  
    }  
      
    public static boolean isOS2(){  
        return OS.indexOf("os/2")>=0;  
    }  
      
    public static boolean isSolaris(){  
        return OS.indexOf("solaris")>=0;  
    }  
      
    public static boolean isSunOS(){  
        return OS.indexOf("sunos")>=0;  
    }  
      
    public static boolean isMPEiX(){  
        return OS.indexOf("mpe/ix")>=0;  
    }  
      
    public static boolean isHPUX(){  
        return OS.indexOf("hp-ux")>=0;  
    }  
      
    public static boolean isAix(){  
        return OS.indexOf("aix")>=0;  
    }  
      
    public static boolean isOS390(){  
        return OS.indexOf("os/390")>=0;  
    }  
      
    public static boolean isFreeBSD(){  
        return OS.indexOf("freebsd")>=0;  
    }  
      
    public static boolean isIrix(){  
        return OS.indexOf("irix")>=0;  
    }  
      
    public static boolean isDigitalUnix(){  
        return OS.indexOf("digital")>=0&&OS.indexOf("unix")>0;  
    }  
      
    public static boolean isNetWare(){  
        return OS.indexOf("netware")>=0;  
    }  
      
    public static boolean isOSF1(){  
        return OS.indexOf("osf1")>=0;  
    }  
      
    public static boolean isOpenVMS(){  
        return OS.indexOf("openvms")>=0;  
    }  
      
    public static String getFullOSName(){
    	return OS;
    }
    
    
    
    /** 
     * 获取操作系统名字 
     * @return 操作系统名 
     */  
    public static OSType getOSName(){  
        if(isAix()){  
        	//不能换成
            _instance.oSType = OSType.AIX;  
        }else if (isDigitalUnix()) {  
            _instance.oSType = OSType.Digital_Unix;  
        }else if (isFreeBSD()) {  
            _instance.oSType = OSType.FreeBSD;  
        }else if (isHPUX()) {  
            _instance.oSType = OSType.HP_UX;  
        }else if (isIrix()) {  
            _instance.oSType = OSType.Irix;  
        }else if (isLinux()) {  
            _instance.oSType = OSType.Linux;  
        }else if (isMacOS()) {  
            _instance.oSType = OSType.Mac_OS;  
        }else if (isMacOSX()) {  
            _instance.oSType = OSType.Mac_OS_X;  
        }else if (isMPEiX()) {  
            _instance.oSType = OSType.MPEiX;  
        }else if (isNetWare()) {  
            _instance.oSType = OSType.NetWare_411;  
        }else if (isOpenVMS()) {  
            _instance.oSType = OSType.OpenVMS;  
        }else if (isOS2()) {  
            _instance.oSType = OSType.OS2;  
        }else if (isOS390()) {  
            _instance.oSType = OSType.OS390;  
        }else if (isOSF1()) {  
            _instance.oSType = OSType.OSF1;  
        }else if (isSolaris()) {  
            _instance.oSType = OSType.Solaris;  
        }else if (isSunOS()) {  
            _instance.oSType = OSType.SunOS;  
        }else if (isWindows()) {  
            _instance.oSType = OSType.Windows;  
        }else{  
            _instance.oSType = OSType.Others;  
        }  
        return _instance.oSType;  
    }  
}  