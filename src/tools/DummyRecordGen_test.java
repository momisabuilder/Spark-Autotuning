/**
 * 
 */
package tools;
/**
*@author tonychao 
*创建时间 ：2018年7月27日 下午7:34:35
*功能
*/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.sql.rowset.FilteredRowSet;

import org.json.JSONException;

import confs.SingleParameter;
import fetcher.SparkConfiguration;
import graybox.ParameterDistrict;
import graybox.ParameterSearch;
import huristic.HuristicConf;
import others.Global;
import others.Global.SparkMode;

/**
 * @author asus
 *
 */
public class DummyRecordGen_test {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public static void main(String[] args) throws IOException, JSONException {
		String path=Global.DUMMY_RECORD_COMMAND_PATH;
		File file=new File(path);
        if(!file.exists()) file.createNewFile();
        FileOutputStream out=new FileOutputStream(file,false); //直接覆盖之前的文件
		
        
        //解析原来的命令文件，准确地说是将元命令文件拆分成两部分，前5行是头，后若干行是尾，在中间插入命令
		BufferedReader br = new BufferedReader(new FileReader(Global.INIT_COMMAND_PATH));
		String s = null;
		int LineCount =0;
		ArrayList<String> head= new ArrayList<String>();
		ArrayList<String> tail =new ArrayList<String>();
		
		while ((s = br.readLine()) != null){// 一行一行读取
			LineCount ++;
			if (LineCount<=5){
				head.add(s);
			}
			else{
				tail.add(s);
			}
		}
		br.close();
		for (int i =0;i<50;i++){ //每个循环生成一条命令
			System.out.println("生成命令:"+i);
			SparkConfiguration conf= new SparkConfiguration();
			ArrayList<String> list = new  ArrayList<>();
			String[] randomParameterList=null;
			
			//遍历每一个参数
			int length0 = Global.CHOSEN_PARAMETERS_GROUP0.length;
			int length1 = Global.CHOSEN_PARAMETERS_GROUP1.length;
			int length2 = Global.CHOSEN_PARAMETERS_GROUP2.length;
			int length3 = Global.CHOSEN_PARAMETERS_GROUP3.length;
			
			randomParameterList =  Arrays.copyOf(Global.CHOSEN_PARAMETERS_GROUP0, length0+length1+length2+length3);//数组扩容并拷贝
		    System.arraycopy(Global.CHOSEN_PARAMETERS_GROUP1, 0, randomParameterList, length0, length1);
			System.arraycopy(Global.CHOSEN_PARAMETERS_GROUP2, 0, randomParameterList, length0+length1, length2);
		    System.arraycopy(Global.CHOSEN_PARAMETERS_GROUP3, 0, randomParameterList, length0+length1+length2, length3);
		    //选择是否全部使用随机参数
		    boolean all_random=false;
		    conf= new ParameterDistrict(new ArrayList<String>(Arrays.asList("group0","group1"))).randomSparkConfiguration(new ArrayList<String>(Arrays.asList("group0","group1")));//第1，2，3，组的参数随机，第0组的参数是默认值 XXXXXX 现在是 都 random
		    
		    //new ArrayList<String>(Arrays.asList("group0","group1"))
		    if (!all_random){
		    	 for (int k=0;k<length0;k++){
		    		 randomParameterList[k]="dummy";
		    	 }
		    	 
		    	 //list.add(e) 
		    	 ArrayList<String> huristicConfList= new HuristicConf(-1,SparkMode.SPARK_ON_YARN).toConfList(); //启发式优化的参数
		    	 for (String str:huristicConfList){
		    		 list.add(str+" \\");
		    	 }
		    	 
		    }
		    
		    
			for (String key: randomParameterList){
				if (key.equals("dummy")) continue;
				SingleParameter parameter=conf.parameterMap.get(key);
				String str="--conf "+parameter.name+"="+parameter.toString()+"  \\";
				list.add(str);
			}
			
			

			//将之前的ArrayList list写进命令文件
			//加文件头
			for (String line:head) addLine(line,out);
			//加文件配置
			for (String str: list){
				addLine(str,out);
			}
			//加文件尾
			for (String line:tail) addLine(line,out);
			//防止冲突
			addLine("wait",out);
			addLine("echo \'########## app 成功执行##############"+"appid:\t"+i+"\'",out);
			addLine("sleep 10",out);
			addLine("",out);
			addLine("",out);
		}
		
		
        
		/**
		*创建时间：2018年7月27日
		*输入
		*输出
		*/
		// TODO Auto-generated method stub
        out.close();

	}

	
	//文件末尾加入一个单词
	public void add(String word,FileOutputStream out) throws UnsupportedEncodingException, IOException{
		if (word!=null) out.write(("\t"+word).getBytes("utf-8"));
		
	}
	
	//向文件末尾中加入一行
	public static void addLine(String line,FileOutputStream out) throws UnsupportedEncodingException, IOException{
		
        if(line!=null) out.write((line+"\n").getBytes("utf-8")); //linux中使用 \n即可 win中可以用 回车换行 \r\n
        
	}

}
