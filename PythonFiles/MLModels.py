# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 10:49:47 2013

@author: 巢泽敏
"""
#当前脚本目录
import sys,os
#print os.path.split( os.path.realpath( sys.argv[0] ) )
ScriptPath = os.path.split( os.path.realpath( sys.argv[0] ) )[0]
ScriptPath=ScriptPath+'\\'

from sklearn.externals import joblib  # save the model in a file

'''
这个函数就是用于提供一系列比较有前途的机器学习模型
返回值：引用的机器学习模型列表
'''
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.tree import ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import GradientBoostingRegressor


def getModelList():#使用的简单模型
    modelList = []

    Linear=False
    #线性回归，确定性模型,但是在某些情况下返回值多一维
    if Linear:
        model = LinearRegression(fit_intercept=True, normalize=False, n_jobs=-1 )
        modelList.append(model);
        model = LinearRegression(fit_intercept=True, normalize=True)
        modelList.append(model);

    #支持向量回归，确定性型模型
    svr=True
    if svr:

        model = SVR(C=1.0, epsilon=0.2)
        modelList.append(model);
        model = SVR(C=1.0, epsilon=0.1)
        modelList.append(model);
        model = SVR(C=1.0, epsilon=0.1,kernel='linear' )
        modelList.append(model);
        model = SVR(C=1.0, epsilon=0.1,kernel='sigmoid' )
        modelList.append(model);

    #外部树回归,随机性
    '''
    This class implements a meta estimator that fits a number of randomized decision trees (a.k.a. extra-trees) 
    on various sub-samples of the dataset and use averaging to improve the predictive accuracy and control over-fitting.
    '''
    extraTree=True
    if extraTree:

        model = ExtraTreeRegressor(max_depth=5,min_samples_split=2)
        modelList.append(model);
        model = ExtraTreeRegressor(max_depth=3,min_samples_split=2)
        modelList.append(model);
        model = ExtraTreeRegressor(max_depth=1,min_samples_split=2)
        modelList.append(model);
        model = ExtraTreeRegressor()
        modelList.append(model);



    #随机数回归，随机模型
    '''
    A random forest regressor.
    A random forest is a meta estimator that fits a number of classifying decision trees on various sub-samples of the dataset
    and use averaging to improve the predictive accuracy and control over-fitting.
    The sub-sample size is always the same as the original input sample size but the samples are drawn with replacement if bootstrap=True (default).
    '''
    randomForestRegressor = True #总体效果不错
    if randomForestRegressor:

        model = RandomForestRegressor(max_depth =4)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =4,n_estimators=40)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =1)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =3)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =3,n_estimators=30)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =1,n_estimators=30)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =3,max_leaf_nodes=100)
        modelList.append(model);
        model = RandomForestRegressor(max_depth =3,max_leaf_nodes=2)
        modelList.append(model);

    '''
    决策树回归
    '''
    tree =True #质量尚可
    if tree:

        model = DecisionTreeRegressor(max_depth =1)
        modelList.append(model);
        model = DecisionTreeRegressor(max_depth =2)
        modelList.append(model);
        model = DecisionTreeRegressor(max_depth =3)
        modelList.append(model);
        model = DecisionTreeRegressor(max_depth =4)
        modelList.append(model);

    '''
    Gradient Boosting for regression.
    GB builds an additive model in a forward stage-wise fashion; 
    it allows for the optimization of arbitrary differentiable loss functions. 
    In each stage a regression tree is fit on the negative gradient of the given loss function.
    '''
    gradientBoostingRegressor =True
    if gradientBoostingRegressor:

        model = GradientBoostingRegressor(n_estimators=200)
        modelList.append(model);
        model = GradientBoostingRegressor(learning_rate=0.05)
        modelList.append(model);
        model = GradientBoostingRegressor()
        modelList.append(model);
    return modelList

