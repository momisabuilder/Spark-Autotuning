### Spark-Autotuning


Apache Spark是一个流行的开源的分布式数据处理平台，它使用分布式内存抽象模型来对分布在集群内存中的数据进行高效地处理。
Apache Spark的性能表现受到配置参数的影响，这些参数的调优往往由人力完成，然而不适当的参数配置可能严重地降低系统的性能表现。
本工具能够根据历史记录自动调整Apache spark的参数设置
### 软件架构说明

```Shell
.
├── conf
│   ├── HIT_tiny_cluster_SearchDistrict.json
│   └── netWorkConf.json
├── LICENSE
├── historyDataWhenDBnotAvaliable
│   ├── Application_Record_418aad6c2653113c77fc9eae20f15af8_cn.ac.ict.bigdatabench.Sort.data
│   ├── Application_Record_464af204c579b99b3665667fc2a82611_KmeansAppJava.data
│   ├── Application_Record_96b59fd590cd7eda7ab0481505b62e89_src.main.scala.pagerankApp.data
│   └── Application_Record_f997016922b609ede885f609f1f3d484_LogisticRegression.src.main.java.LogisticRegressionApp.data
├── PythonFiles
│   ├── daemon_fromDB.py
│   ├── daemon_from_File2.py
│   ├── dataFileReader.py
│   ├── DBReader.py
│   ├── MLModels.py
│   ├── trainModelFromData2_only_use_SVM.py
│   ├── trainModelFromData2.py
│   └── trainModelFromDB.py
└── src
    ├── confs
    │   ├── DataType.java
    │   ├── ParameterGroup.java
    │   └── SingleParameter.java
    ├── fetcher
    │   ├── APIFetcher.java
    │   ├── HTTPWebFetcher.java
    │   ├── RDDInfo.java
    │   ├── RunRecord.java
    │   ├── SparkConfiguration.java
    │   ├── StageInfo.java
    │   └── TaskInfo.java
    ├── FileIO
    │   └── OutPutFile.java
    ├── gradientDescent
    ├── graybox
    │   ├── CallPythonWindows.java
    │   ├── GrayBoxConf.java
    │   ├── ParameterDistrict.java
    │   └── ParameterSearch.java
    ├── huristic
    │   ├── Estimator.java
    │   ├── HuristicConf.java
    │   ├── StandAloneResourceEstimator.java
    │   ├── yarnResourceEstimator.java
    │   └── zlocal_test.java
    ├── mysql
    │   ├── DataFile.java
    │   └── DB.java
    ├── others
    │   ├── DataType.java
    │   └── Global.java
    ├── shellInterface
    │   ├── LinuxShell.java
    │   ├── OSinfo.java
    │   ├── OSType.java
    │   ├── Temp.java
    │   └── UserSubmitInterface_test.java
    └── tools
        └── DummyRecordGen_test.java

```
### 环境要求
JDK 1.8.0
Hadoop:2.7.0
Spark :2.1.0 （on yarn 模式）
Python 2.7.0
请确保python已经安装以下版本的包
MySQL-python                       1.2.5        
numpy                              1.13.1         
Pillow                             4.2.1             
scikit-learn                       0.18.2     
scipy                              0.19.1        
  
### 安装
1.  本工具需要在已经安装Apache spark 的环境下进行工作，在Apache Spark安装完成后，需要开启Spark历史服务器，并在配置文件中配置历史服务器对应地址。
2.  将java源代码打成编译为jar包之后，放在工程根目录下即可

### 使用说明

1.  命令格式： java -jar 编译得到的jar包名.jar + spark 命令 

```Shell
Java -jar Spark-tune-submit.jar [options]  <app jar | python file>  [app arguments]
```

| 参数字段              | 描述                                       | 其他                                                   |                  |
| --------------------- | ------------------------------------------ | ------------------------------------------------------ | ---------------- |
| Spark-tune-submit.jar | 优化模块程序包                             |                                                        |                  |
| [options]             | Spark原生系统的选项，如参数，运行模式等    | 可选                                                   |                  |
| <app jar 或 python file>                               | 用户向系统提交的可执行文件，可以是jar包或python 脚本。 | 需要使用绝对路径 |
| [app arguments]       | 用户提交的jar包或者python 脚本的输入参数。 | 可选                                                   |                  |

![示例](https://images.gitee.com/uploads/images/2019/1028/103048_f74cb0cc_5327108.png "tmp.png")

2.  根据不同的历史数据积累情况，以及用户的选择，本工具将采用不同的优化模式进行调优，用户可以拒绝对应用进行优化。

### 配置文件

1.   [主要配置文件](https://gitee.com/HITMassiveData/Spark-Autotuning/blob/master/conf/netWorkConf.json)
2.   [配置参数搜索空间](https://gitee.com/HITMassiveData/Spark-Autotuning/blob/master/conf/HIT_tiny_cluster_SearchDistrict.json)
3.   [部分配置由于没有什么改动的需求，直接使用硬编码方式，如需调整可以在编译前修改](https://gitee.com/HITMassiveData/Spark-Autotuning/blob/master/src/others/Global.java)
